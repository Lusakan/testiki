package cm;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager{


    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        Spaceship mostPowerfulShip = null;
        int maxFirepower = 0;
        for (Spaceship sp:ships) {
            if(sp.getFirePower() > maxFirepower){
                mostPowerfulShip = sp;
                maxFirepower = sp.getFirePower();
            }
        }
        return mostPowerfulShip;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (Spaceship ship:ships) {
            if(ship.getName().equals(name)){
                return ship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> res = new ArrayList<>();
        for (Spaceship sp:ships) {
            if(sp.getCargoSpace() >= cargoSize){
                res.add(sp);
            }
        }
        return res;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> res = new ArrayList<>();
        for (Spaceship sp:ships) {
            if(sp.getFirePower()<=0){
                res.add(sp);
            }
        }
        return res;
    }
}
