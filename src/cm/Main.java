package cm;

public class Main {

    public static void main(String[] args) {

        SpaceshipFleetManagerTest spTest = new SpaceshipFleetManagerTest();

        double test1 = spTest.testGetAllCivilanShipsMethod();
        double test2 = spTest.testShipWithEnoughCargoSpaceMethod();
        double test3 = spTest.testNameMethod();
        double test4 = spTest.testGetMostPowerfulShip();
        double res = (double) ((int)((test1 + test2 + test3 + test4)*100))/100;

        System.out.println("\n\nИтоговый балл = " + res);
    }
}
