package cm;

import java.util.ArrayList;


public class SpaceshipFleetManagerTest {

    CommandCenter commandCenter = new CommandCenter();
    ArrayList<Spaceship> testShipsList = new ArrayList<>();

    public double testNameMethod(){
        double res = 0.72;
        boolean testName1 = getShipByName_ShipWithNameExist_returnsIsShipWithNameExist();
        boolean testName2 = getShipByName_ShipWithNameDoesntExist_returnsIsShipWithNameExist();

        if(!testName1){
            System.out.println("Не возвращает корабль по заданному имени");
            res-=0.36;
        }
        if(!testName2){
            System.out.println("Не возвращает null когда корабля нет");
            res-=0.36;
        }
        if(testName1&&testName2){
            System.out.println("Метод getShipByName работает хорошо");
        }
        return res;
    }

    public double testShipWithEnoughCargoSpaceMethod(){
        boolean test1 = getAllShipsWithEnoughCargoSpace_AllShipsWithEqualEnoughMinCargoSpace_returnsIsMethodReturnsRightArrayList();
        boolean test2 = getAllShipsWithEnoughCargoSpace_AllShipsWithLessCargoSpace_returnsIsMethodReturnsRightArrayList();
        boolean test3 = getAllShipsWithEnoughCargoSpace_OneNotFirstShipHaveEnoughCargoSpace_returnsIsMethodReturnsRightArrayList();
        double res = 1.08;

        if(!test1){
            System.out.println("Тест: Все корабли с одинаковым багажным отделением, которые по объему такие же как и " +
                    "требуемое значение\n" +
                    "Результат: Не пройден");
            res-=0.36;
        }
        if(!test2){
            System.out.println("Тест: Все корабли с одинаковым багажным отделением, которые по объему меньше чем " +
                    "требуемое значение\n" +
                    "Результат: Не пройден");
            res-=0.36;

        }if(!test3){
            System.out.println("Тест: только у одного корабля достаточно большое багажное отделение требуемое значение\n" +
                    "Результат: Не пройден");
            res-=0.36;

        }if(test1&&test2&&test3){
            System.out.println("Метод getAllShipsWithEnoughCargoSpace работает хорошо");
        }
        return res;
    }

    public double testGetAllCivilanShipsMethod(){
        boolean test1 = getAllCivilianShips_AllShipsAreCivilian_returnsIsMethodReturnRightArrayList();
        boolean test2 = getAllCivilianShips_NoCivilianShips_returnsIsMethodReturnRightArrayList();
        boolean test3 = getAllCivilianShips_OneNotFirstShipIsCivilian_returnsIsMethodReturnRightArrayList();
        double res = 1.08;

        if(!test1){
            System.out.println("Тест: Все корабли мирные\n" +
                    "Результат: Не пройден");
            res-=0.36;

        }
        if(!test2){
            System.out.println("Тест: Все корабли боевые\n" +
                    "Результат: Не пройден");
            res-=0.36;

        }if(!test3){
            System.out.println("Тест: Только один мирный корабль\n" +
                    "Результат: Не пройден");
            res-=0.36;

        }if(test1&&test2&&test3){
            System.out.println("Метод getAllCivilianShips работает хорошо");
        }
        return res;
    }

    public double testGetMostPowerfulShip(){
        boolean test1 = getMostPowerfulShip_AllShipsCivilian_returnsIsMethodReturnsMostPowerfulShip();
        boolean test2 = getMostPowerfulShip_AllShipsSameFirepower_returnsIsMethodReturnsMostPowerfulShip();
        boolean test3 = getMostPowerfulShip_OneNotFirstShipHasMaxFirepower_returnsIsMethodReturnsMostPowerfulShip();

        double res = 1.11;
        if(!test1){
            System.out.println("Тест: Bсе корабли мирные \nРезультат: не пройден");
            res-=0.37;
        }
        if(!test2){
            System.out.println("Тест: У всех кораблей одинаковая мощь \nРезультат: не пройден");
            res-=0.37;
        }
        if(!test3){
            System.out.println("Тест: Один корабль самый сильный \nРезультат: не пройден");
            res-=0.37;
        }
        if(test1&&test2&&test3){
            System.out.println("Метод getMostPowerfulShip работает хорошо");
        }

        return res > 0 ? res + 0.01 : res;
    }

    private boolean getShipByName_ShipWithNameExist_returnsIsShipWithNameExist(){
        testShipsList.add(new Spaceship("George", 0, 0, 0));
        testShipsList.add(new Spaceship("Alan", 0, 0, 0));
        testShipsList.add(new Spaceship("Mike", 0, 0, 0));

        Spaceship result = commandCenter.getShipByName(testShipsList, "Alan");
        if (result == testShipsList.get(1)){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }
    private boolean getShipByName_ShipWithNameDoesntExist_returnsIsShipWithNameExist(){
        testShipsList.add(new Spaceship("George", 0, 0, 0));
        testShipsList.add(new Spaceship("Alan", 0, 0, 0));
        testShipsList.add(new Spaceship("Mike", 0, 0, 0));

        Spaceship result = commandCenter.getShipByName(testShipsList, "Max");
        if (result == null){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_AllShipsWithEqualEnoughMinCargoSpace_returnsIsMethodReturnsRightArrayList(){
        testShipsList.add(new Spaceship("You", 0, 100, 0));
        testShipsList.add(new Spaceship("Was", 0, 100, 0));
        testShipsList.add(new Spaceship("Rickrolled", 0, 100, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 100);
        if(result.equals(testShipsList)){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }
    private boolean getAllShipsWithEnoughCargoSpace_AllShipsWithLessCargoSpace_returnsIsMethodReturnsRightArrayList(){
        testShipsList.add(new Spaceship("You", 0, 100, 0));
        testShipsList.add(new Spaceship("Was", 0, 100, 0));
        testShipsList.add(new Spaceship("Rickrolled", 0, 100, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 120);
        if(result.isEmpty()){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }
    private boolean getAllShipsWithEnoughCargoSpace_OneNotFirstShipHaveEnoughCargoSpace_returnsIsMethodReturnsRightArrayList(){
        testShipsList.add(new Spaceship("Was", 0, 10, 0));
        testShipsList.add(new Spaceship("You", 0, 100, 0));
        testShipsList.add(new Spaceship("Rickrolled", 0, 10, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 90);
        if(result.contains(testShipsList.get(1)) && result.size() == 1){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }

    private boolean getAllCivilianShips_AllShipsAreCivilian_returnsIsMethodReturnRightArrayList(){
        testShipsList.add(new Spaceship("You", 0, 0, 0));
        testShipsList.add(new Spaceship("Was", 0, 0, 0));
        testShipsList.add(new Spaceship("Rickrolled", 0, 0, 0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        if(result.equals(testShipsList)){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }
    private boolean getAllCivilianShips_NoCivilianShips_returnsIsMethodReturnRightArrayList(){
        testShipsList.add(new Spaceship("You", 1, 0, 0));
        testShipsList.add(new Spaceship("Was", 1, 0, 0));
        testShipsList.add(new Spaceship("Rickrolled", 1, 0, 0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        if(result.isEmpty()){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }
    private boolean getAllCivilianShips_OneNotFirstShipIsCivilian_returnsIsMethodReturnRightArrayList(){
        testShipsList.add(new Spaceship("You", 1, 0, 0));
        testShipsList.add(new Spaceship("Was", 0, 0, 0));
        testShipsList.add(new Spaceship("Rickrolled", 1, 0, 0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        if(result.contains(testShipsList.get(1)) && result.size() == 1){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }

    private boolean getMostPowerfulShip_AllShipsSameFirepower_returnsIsMethodReturnsMostPowerfulShip(){
        testShipsList.add(new Spaceship("Alex", 100, 0, 0));
        testShipsList.add(new Spaceship("John", 100, 0, 0));
        testShipsList.add(new Spaceship("42", 100, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);
        if(result == testShipsList.get(0)){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }
    private boolean getMostPowerfulShip_AllShipsCivilian_returnsIsMethodReturnsMostPowerfulShip(){
        testShipsList.add(new Spaceship("Alex", 0, 0, 0));
        testShipsList.add(new Spaceship("John", 0, 0, 0));
        testShipsList.add(new Spaceship("42", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);
        if(result == null){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }
    private boolean getMostPowerfulShip_OneNotFirstShipHasMaxFirepower_returnsIsMethodReturnsMostPowerfulShip(){
        testShipsList.add(new Spaceship("Alex", 100, 0, 0));
        testShipsList.add(new Spaceship("John", 1000, 0, 0));
        testShipsList.add(new Spaceship("42", 100, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);
        if(result == testShipsList.get(1)){
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }
}
