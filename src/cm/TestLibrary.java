package cm;

import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class TestLibrary {

    private static ArrayList<Spaceship> ar = new ArrayList<>();
    private CommandCenter cm = new CommandCenter();


    @DisplayName("Тест на правильность возврата самого мощного корабля")
    @Test
    void mostPowerfulShip_returnsMostPowerfulShip(){
        ar.add(new Spaceship("Krokoloko", 100, 10,10));
        ar.add(new Spaceship("Voyajer-1", 120, 10,10));
        ar.add(new Spaceship("Voyajer-2", 0, 10,10));
        Assertions.assertEquals(ar.get(1), cm.getMostPowerfulShip(ar));
        ar.clear();
    }


    @DisplayName("Тест на возврат null при отсутствии самого мощного корабля")
    @Test
    void mostPowerfulShip_returnsNull(){
        ar.add(new Spaceship("Voyajer-2", 0, 10,10));
        ar.add(new Spaceship("Millenium Falcon", 0, 10,10));
        ar.add(new Spaceship("Centurion", 0, 0,0));
        Assertions.assertNull(cm.getMostPowerfulShip(ar));
        ar.clear();
    }

    @DisplayName("Возвращает первый по списку мощный корабль, если у всех одинаковая мощность > 0")
    void mostPowerfulShip_returnsFirst(){
        ar.add(new Spaceship("Voyajer-2", 100, 10,10));
        ar.add(new Spaceship("Millenium Falcon", 100, 10,10));
        ar.add(new Spaceship("Centurion", 100, 0,0));
        Assertions.assertEquals(ar.get(0), cm.getMostPowerfulShip(ar));
        ar.clear();
    }


    @DisplayName("Тест на возврат всех мирных кораблей")
    @Test
    void getAllCivilianShips_arrayWithCivilianShips(){
        ar.add(new Spaceship("Voyajer-2", 0, 10,10));
        ar.add(new Spaceship("Millenium Falcon", 0, 10,10));
        ar.add(new Spaceship("Centurion", 0, 10,0));
        Assertions.assertEquals(ar, cm.getAllCivilianShips(ar));
        ar.clear();
    }

    @DisplayName("Тест на отсутствие мирных кораблей")
    @Test
    void getAllCivilianShips_arrayWithoutCivilianShips(){
        ar.add(new Spaceship("Krokoloko", 100, 10,10));
        ar.add(new Spaceship("Voyajer-1", 120, 10,10));
        Assertions.assertEquals(new ArrayList<Spaceship>(),cm.getAllCivilianShips(ar));
        ar.clear();
    }

    @DisplayName("Тест на наличие корабля по имени")
    @Test
    void getShipByName_shipExist(){
        ar.add(new Spaceship("Krokoloko", 100, 10,10));
        ar.add(new Spaceship("Voyajer-1", 120, 10,10));
        Assertions.assertEquals(ar.get(1), cm.getShipByName(ar,"Voyajer-1"));
        ar.clear();
    }

    @DisplayName("Тест на отсутствие корабля по имени")
    void getShipByName_shipDoesntExist(){
        Assertions.assertNull(cm.getShipByName(ar, "Oleg"));
    }

    @DisplayName("Тест на корабли с достаточным количеством свободного места, когда они есть")
    @Test
    void getAllShipsWithEnoughCargoSpace_ShipsExists(){
        ar.add(new Spaceship("Krokoloko", 100, 10,10));
        ar.add(new Spaceship("Voyajer-1", 120, 10,10));
        Assertions.assertEquals(ar, cm.getAllShipsWithEnoughCargoSpace(ar, 10));
        ar.clear();
    }

    @DisplayName("Тест на корабли с достаточным количеством свободного места, когда их нет")
    @Test
    void getAllShipsWithEnoughCargoSpace_ShipsWithoutEnoughCargoSpace() {
        ar.add(new Spaceship("Voyajer-2", 0, 10,10));
        ar.add(new Spaceship("Millenium Falcon", 0, 10,10));
        ar.add(new Spaceship("Centurion", 0, 10,0));
        Assertions.assertEquals(new ArrayList<Spaceship>(), cm.getAllShipsWithEnoughCargoSpace(ar, 11));
        ar.clear();
    }
}
